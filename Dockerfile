FROM golang:latest

RUN mkdir app

WORKDIR /app

COPY . gitlab.com/vyntt/app/assignment1
RUN cd gitlab.com/vyntt/app/assignment1 && go mod download all


RUN cd gitlab.com/vyntt/app/assignment1 && go build -o /crud

EXPOSE 55001

CMD [ "/crud" ]
