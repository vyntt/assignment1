package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/vyntt/assignment1/ent"
	"gitlab.com/vyntt/assignment1/ent/user"
	pb "gitlab.com/vyntt/assignment1/protos"
	"google.golang.org/grpc"
)

var (
	port = flag.Int("port", 55001, "The server port")
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedCRUDUserServer
	db *ent.Client
}

func (s *server) CreateUser(ctx context.Context, in *pb.CreateUserRequest) (*pb.CreateUserReply, error) {
	log.Println("CREATE: ---------", in.GetUser().GetName(), in.GetUser().GetAge())
	fmt.Println("Process to create user..............")
	fmt.Println("Start to create user")
	res, err := s.db.User.
		Create().
		SetAge(in.User.Age).
		SetName(in.User.Name).
		Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating user: %w", err)
	}
	fmt.Println("Add successful")
	fmt.Println(res.Age, res.Name)
	var reply *pb.CreateUserReply = &pb.CreateUserReply{
		User: &pb.User{
			Age:  int64(res.Age),
			Name: res.Name,
		},
	}
	return reply, nil
}

func (s *server) UpdateUser(ctx context.Context, in *pb.UpdateUserRequest) (*pb.UpdateUserReply, error) {
	log.Printf("UPDATE: %v------------", in.GetIdxUpdate())
	var available, err1 = s.db.User.Query().Where(user.ID(int(in.IdxUpdate))).All(ctx)
	if err1 != nil || len(available) == 0 {
		return nil, fmt.Errorf("failed search user: %w", err1)
	}
	var _, _ = s.db.User.UpdateOneID(int(in.IdxUpdate)).SetName(in.NewInfo.Name).SetAge(in.NewInfo.Age).Save(ctx)
	fmt.Println("Update successful")
	var new, err3 = s.db.User.Query().Where(user.ID(int(in.IdxUpdate))).All(ctx)
	if err3 != nil || len(available) == 0 {
		return nil, fmt.Errorf("failed search user: %w", err3)
	}
	var reply *pb.UpdateUserReply = &pb.UpdateUserReply{
		NewUser: &pb.User{
			Age:  new[0].Age,
			Name: new[0].Name,
		},
	}
	return reply, nil
}

func (s *server) DeleteUser(ctx context.Context, in *pb.DeleteUserRequest) (*pb.DeleteUserReply, error) {
	log.Printf("DELETE: %v------------", in.GetIdxDelete())
	var available, err1 = s.db.User.Query().Where(user.ID(int(in.IdxDelete))).All(ctx)
	if err1 != nil || len(available) == 0 {
		return nil, fmt.Errorf("failed search user: %w", err1)
	}
	var err2 = s.db.User.DeleteOneID(int(in.GetIdxDelete())).Exec(ctx)
	if err2 != nil {
		return nil, fmt.Errorf("failed delete user: %w", err2)
	}
	fmt.Println("Delete successful")
	var res = available[0]
	var reply *pb.DeleteUserReply = &pb.DeleteUserReply{
		User: &pb.User{
			Age:  res.Age,
			Name: res.Name,
		},
	}
	return reply, nil
}

func (s *server) ListUsers(ctx context.Context, in *pb.ListUsersRequest) (*pb.ListUsersReply, error) {

	log.Printf("LISTEN------------")
	var users, err = s.db.User.Query().All(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed query user: %w", err)
	}
	fmt.Println("Query successful")
	var userReply []*pb.User
	var size = len(users)
	for i := 0; i < size; i++ {
		userReply = append(userReply, &pb.User{
			Age:  users[i].Age,
			Name: users[i].Name,
		})
	}
	var reply *pb.ListUsersReply = &pb.ListUsersReply{
		Users: userReply,
	}
	return reply, nil
}

func main() {
	flag.Parse()
	db, err := ent.Open("mysql", "root:TuVy2709@@tcp(172.17.0.2:3306)/user")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer db.Close()
	// Run the auto migration tool.
	if err := db.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	// fmt.Println("CONNECT SUCCESSFUL")
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterCRUDUserServer(s, &server{db: db})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
